package com.cskaoyan.servlet;

import com.cskaoyan.bean.common.BaseParam;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.AddressService;
import com.cskaoyan.service.AddressServiceImpl;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/admin/address/*")
public class AdminAddressServlet extends WdBaseServlet {

    AddressService addressService = new AddressServiceImpl();


    @SneakyThrows
    public BaseRespVo list(HttpServletRequest request, HttpServletResponse response) {
        BaseParam baseParam = new BaseParam();
        String userIdStr = request.getParameter("userId");
        Integer userId = userIdStr == null? null : Integer.valueOf(userIdStr);
        String name = request.getParameter("name");

        BeanUtils.copyProperties(baseParam, request.getParameterMap());
        CommonData data = addressService.queryAddressList(baseParam, userId, name);
        return BaseRespVo.ok(data);
    }
}
