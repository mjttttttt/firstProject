package com.cskaoyan.servlet;

import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.mapper.MarketStorageMapper;
import com.cskaoyan.service.AdminStorageService;
import com.cskaoyan.service.AdminStorageServiceImpl;
import com.cskaoyan.util.MyBatisUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

@MultipartConfig
@WebServlet("/admin/storage/*")
public class AdminStorageServlet extends WdBaseServlet {

    AdminStorageService adminStorageService = new AdminStorageServiceImpl();

    @SneakyThrows
    public BaseRespVo create(HttpServletRequest request, HttpServletResponse response) {
        String storagePath = (String) getServletContext().getAttribute("storagePath");
        Part part = request.getPart("file");

        MarketStorage marketStorage = adminStorageService.saveAndCreateRecord(storagePath, part);
        return BaseRespVo.ok(marketStorage);
    }


}
