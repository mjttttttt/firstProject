package com.cskaoyan.util;

/**
 * @author stone
 * @date 2023/03/17 19:48
 */
public class StringUtil {
    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
    public static boolean isNull(String str) {
        return str == null;
    }
    public static boolean isNotNull(String str) {
        return str != null;
    }
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

}
