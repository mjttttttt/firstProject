package com.cskaoyan.util;

import com.cskaoyan.bean.common.BaseRespVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author stone
 * @date 2023/03/17 11:02
 */
public class ResponseUtil {

    public static void responseJson(HttpServletResponse response, Object instance) throws IOException {
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().println(JacksonUtil.write(instance));
    }
    public static void responseOkJsonVo(HttpServletResponse response, Object data) throws IOException {
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().println(JacksonUtil.write(BaseRespVo.ok(data)));
    }
}
