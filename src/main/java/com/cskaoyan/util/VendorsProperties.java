package com.cskaoyan.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author stone
 * @date 2023/03/19 15:46
 */
public class VendorsProperties {
    private static List<Map<String, String>> vendors=new ArrayList<>();
    static {
        InputStream inputStream = VendorsProperties.class.getClassLoader().getResourceAsStream("express_vendors.properties");
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(inputStream,"utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            String value = properties.getProperty(key);
            HashMap<String, String> map = new HashMap<>();
            map.put("code", key);
            map.put("name",value);
            vendors.add(map);
        }
    }

    public static List<Map<String, String>> getVendors() {
        return vendors;
    }
}
