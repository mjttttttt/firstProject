package com.cskaoyan.util;

import lombok.SneakyThrows;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Properties;

/**
 * @author stone
 * @date 2023/03/19 21:31
 */
public class LocalStorage {
    @SneakyThrows
    public void store(InputStream inputStream, long size, String contentType, String key) {
        InputStream configInputStream = LocalStorage.class.getClassLoader().getResourceAsStream("config.properties");
        Properties properties = new Properties();
        properties.load(configInputStream);
        String path = properties.getProperty("storage.path");
        try {
            Path rootLocation = Paths.get(path);
            //Files.createDirectory(rootLocation);
            Files.copy(inputStream, rootLocation.resolve(key), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Failed to store file " + key, e);
        }
    }
}
