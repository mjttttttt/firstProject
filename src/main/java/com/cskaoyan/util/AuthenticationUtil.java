package com.cskaoyan.util;

import com.cskaoyan.exception.InfoExpiredException;
import com.cskaoyan.exception.NoInfoException;

/**
 * @author stone
 * @date 2023/03/17 14:09
 */
public class AuthenticationUtil {
    private static ThreadLocal<String> localVar = new ThreadLocal<String>();

    public static Object getPrincipal() throws NoInfoException, InfoExpiredException {
        String token = localVar.get();
        Object info = WdTokenHolder.getInfo(token);
        return info;
    }

    public static void setToken(String token) {
        localVar.set(token);
    }
}
