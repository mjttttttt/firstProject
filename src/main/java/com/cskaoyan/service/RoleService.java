package com.cskaoyan.service;

import java.util.Set;

/**
 * @author stone
 * @date 2023/03/17 14:24
 */
public interface RoleService {
    Set<String> queryByIds(Integer[] roleIds);
}
