package com.cskaoyan.service;

import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.bean.MarketAdminExample;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * @author stone
 * @date 2023/03/17 10:32
 */
public class AdminServiceImpl implements AdminService {
    SqlSessionFactory sqlSessionFactory = MyBatisUtil.getSqlSessionFactory();

    @Override
    public MarketAdmin query(String username, String password) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MarketAdminMapper mapper = sqlSession.getMapper(MarketAdminMapper.class);
        MarketAdminExample example = new MarketAdminExample();
        example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        List<MarketAdmin> marketAdmins = mapper.selectByExample(example);
        sqlSession.commit();
        if (marketAdmins != null && marketAdmins.size() == 1) {
            return marketAdmins.get(0);
        }
        return null;
    }
}
