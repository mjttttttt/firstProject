package com.cskaoyan.service;

import com.cskaoyan.bean.MarketRole;
import com.cskaoyan.bean.MarketRoleExample;
import com.cskaoyan.mapper.MarketRoleMapper;
import com.cskaoyan.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author stone
 * @date 2023/03/17 14:24
 */
public class RoleServiceImpl implements RoleService {
    SqlSessionFactory sqlSessionFactory = MyBatisUtil.getSqlSessionFactory();
    @Override
    public Set<String> queryByIds(Integer[] roleIds) {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        MarketRoleMapper roleMapper = sqlSession.getMapper(MarketRoleMapper.class);
        Set<String> roles = new HashSet<String>();
        if(roleIds.length == 0){
            return roles;
        }

        MarketRoleExample example = new MarketRoleExample();
        example.or().andIdIn(Arrays.asList(roleIds)).andEnabledEqualTo(true).andDeletedEqualTo(false);
        List<MarketRole> roleList = roleMapper.selectByExample(example);
        sqlSession.commit();
        for(MarketRole role : roleList){
            roles.add(role.getName());
        }

        return roles;
    }
}
