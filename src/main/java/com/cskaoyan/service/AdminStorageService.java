package com.cskaoyan.service;

import com.cskaoyan.bean.MarketStorage;

import javax.servlet.http.Part;

public interface AdminStorageService {
    MarketStorage saveAndCreateRecord(String storagePath, Part part);
}
