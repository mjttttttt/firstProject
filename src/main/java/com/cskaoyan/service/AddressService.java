package com.cskaoyan.service;

import com.cskaoyan.bean.common.BaseParam;
import com.cskaoyan.bean.common.CommonData;

public interface AddressService {

    CommonData queryAddressList(BaseParam baseParam, Integer userId, String name);
}
