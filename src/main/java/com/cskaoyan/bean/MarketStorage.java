package com.cskaoyan.bean;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class MarketStorage {
    private Integer id;

    private String key;

    private String name;

    private String type;

    private Integer size;

    private String url;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

}