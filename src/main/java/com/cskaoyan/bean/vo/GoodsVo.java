package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.MarketGoods;
import com.cskaoyan.bean.MarketGoodsAttribute;
import com.cskaoyan.bean.MarketGoodsProduct;
import com.cskaoyan.bean.MarketGoodsSpecification;
import lombok.Data;

/**
 * @author stone
 * @date 2023/03/20 09:37
 */
@Data
public class GoodsVo {
    MarketGoods goods;
    MarketGoodsSpecification[] specifications;
    MarketGoodsAttribute[] attributes;
    MarketGoodsProduct[] products;
}
