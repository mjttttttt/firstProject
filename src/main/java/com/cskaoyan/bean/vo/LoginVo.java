package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author stone
 * @date 2023/04/05 11:27
 */
@NoArgsConstructor
@Data
public class LoginVo {

    private Integer errno;
    private DataBean data;
    private String errmsg;

    @NoArgsConstructor
    @Data
    public static class DataBean {
        private AdminInfoBean adminInfo;
        private String token;

        @NoArgsConstructor
        @Data
        public static class AdminInfoBean {
            private String nickName;
            private String avatar;
        }
    }
}
